import React,{Component} from 'react';
import './App.css';
import {BrowserRouter as Router,Switch,Route} from 'react-router-dom'
import 'bootstrap/dist/css/bootstrap.min.css';
import Main from './Component/Main';
import Menu from './Component/Menu';
import Add from './Component/Add';
import View from './Component/View';
import Edit from './Component/Edit';
import Search from './Component/Search';

export default class App extends Component {
  render(){
    return(
      <div className="App">
        <Router>
          <Menu/>
          <Switch>
            <Route path='/' exact component={Main}/>
            <Route path='/add' exact component={Add}/>
            <Route path='/view/:id' exact component={View}/>
            <Route path='/edit/:id' exact component={Edit}/>
            <Route path='/search/:title' exact component={Search}/>
          </Switch>
        </Router>
      </div>
    )
  }
}
