import React from 'react'
import {Form,Button,Container,Row,Col,Image} from 'react-bootstrap'
import axios from 'axios';
import swal from 'sweetalert';
import {Link} from 'react-router-dom'
export default class Edit extends React.Component {
    constructor(props){
        super(props);
        this.state={
            data: {
                DATA: []
            },
            TITLE: '',
            DESCRIPTION: '',
            IMAGE: '',
            validateTitle: '',
            validateDesc: ''
        }
    }
    onHandleTitle=(e)=>{
        this.setState({
            TITLE: e.target.value
        })
    }
    onHandleDsec=(e)=>{
        this.setState({
            DESCRIPTION: e.target.value
        })   
    }
    onImageChange = e => {
        console.log(e);
        if (e.target.files && e.target.files[0]) {
          this.setState({
            IMAGE: URL.createObjectURL(e.target.files[0]),
          });
        }
    };
    fileSelectedHandler=(e)=>{
        this.refs.add.click();
    }
    handleValidation(){
        let fields = this.state;
        let isValid = true;
        if(!fields["TITLE"]){
          isValid = false;
          this.setState({validateTitle: "* Title cannot be empty"})
        }
        if(!fields["DESCRIPTION"]){
          isValid = false;
          this.setState({validateDesc: "* Description cannot be empty"})
        }
        return isValid;
    }  
    onSubmit=(e)=>{
        e.preventDefault();
            if(this.handleValidation()){
            const ID = this.props.match.params.id;
            const {TITLE,DESCRIPTION,IMAGE}=this.state
            console.log(TITLE);
            console.log(DESCRIPTION);
            axios.put(`http://110.74.194.124:15011/v1/api/articles/${ID}`,{
                TITLE,
                DESCRIPTION,
                IMAGE
            })
            .then(res=>{
                swal("Update Successfully!", "Data has been updated", "success");
                window.history.back()
            })
            .catch(err=>
                console.log(err)
            )
        }
    }
    componentWillMount(){
        const ID = this.props.match.params.id;
        console.log(this.props.match.params.id);
        axios.get(`http://110.74.194.124:15011/v1/api/articles/${ID}`).then(res =>{
            this.setState({
                data: res.data,
                TITLE: res.data.DATA.TITLE,
                DESCRIPTION: res.data.DATA.DESCRIPTION,
                IMAGE: res.data.DATA.IMAGE
            });
            console.log(this.state.data)
            console.log(this.state.title)
            console.log(this.state.desc)
            console.log(this.state.IMAGE)
        })
    }
    render(){
        return(
            <Container style={{textAlign: 'start'}}>
                <h1 style={{marginTop: '3%'}}>Update Article</h1>
                <Row>
                    <Col lg="6">
                        <Form id="myform" style={{marginTop: '2%'}}>
                            <Form.Group>
                                <Form.Label>TITLE <span style={{ fontSize: 16, color: "red" }}>{this.state.validateTitle}</span></Form.Label>
                                <Form.Control type="text" placeholder="Title" name="TITLE" value={this.state.TITLE} onChange={this.onHandleTitle}/>
                            </Form.Group>

                            <Form.Group>
                            <Form.Label>DESCRIPTION <span style={{ fontSize: 16, color: "red" }}>{this.state.validateDesc}</span></Form.Label>
                                <Form.Control type="text" placeholder="Description" name="DESCRIPTION" value={this.state.DESCRIPTION} onChange={this.onHandleDsec}/>
                            </Form.Group>

                            <Button style={{marginRight: 5}} variant="dark" value="submit" onClick={this.onSubmit} >Submit</Button>
                            <Link to='/'><Button variant="danger">Back</Button></Link>
                        </Form>
                    </Col>
                    <Col lg="6">
                        <div style={{marginTop: '2%'}}>
                            <input style={{display: "none"}} type="file" ref="add"  onChange={this.onImageChange}/>
                            <Image id="img" src={this.state.IMAGE} onClick={this.fileSelectedHandler} alt="no image" thumbnail/>
                        </div>
                    </Col>
                </Row>
            </Container>
        )
    }
}
