import React from 'react'
import { Table, Button, Container,Image } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import axios from 'axios'
import Pagination from "react-js-pagination";
import swal from 'sweetalert';

export default class Main extends React.Component {
    constructor(){
        super();
        this.state = {
            data: {
                DATA: []
            },
            activePage: 1,
            perPage: 5,
        }
    };
    componentWillMount(){
        axios.get(`http://110.74.194.124:15011/v1/api/articles?page=1&limit=15`).then(res => {
            this.setState({data: res.data});
        });
    }
    componentWillUpdate(){
        axios.get(`http://110.74.194.124:15011/v1/api/articles?page=1&limit=15`).then(res => {
            this.setState({data: res.data});
        });
    }
    handleDelete=(id)=>{
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this data!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
                axios.delete(`http://110.74.194.124:15011/v1/api/articles/${id}`).then(res=>{
                    this.componentWillMount()
                })
              swal("Data has been deleted!", {
                icon: "success",
              });
            } else {
              swal("Data is safe!");
            }
          });
        // axios.delete(`http://110.74.194.124:15011/v1/api/articles/${id}`).then(res=>{
        //     this.componentWillMount()
        // })
    }
    handlePageChange(pageNumber) {
        console.log(`active page is ${pageNumber}`);
        this.setState({activePage: pageNumber});
    }
    convertDate=(dateStr)=>{
        var dateString = dateStr;
        var year = dateString.substring(0, 4);
        var month = dateString.substring(4, 6);
        var day = dateString.substring(6, 8);
        var date = year + "-" + month + "-" + day;
        return date;
    }
    render(){
        const indexOfLast =this.state.activePage * this.state.perPage;
        const indexOfFirst = indexOfLast - this.state.perPage;
        const current = this.state.data.DATA.slice(indexOfFirst, indexOfLast);
        const myData = current.map((d)=>
            <tbody key={d.ID}>
                <tr>
                    <td>{d.ID}</td>
                    <td style={{width: '20%'}}>{d.TITLE}</td>
                    <td style={{width: '25%'}}>{d.DESCRIPTION}</td>
                    <td style={{width: '10%'}}>{this.convertDate(d.CREATED_DATE)}</td>
                    <td style={{width: '15%'}}>
                        <Image src={d.IMAGE} thumbnail />
                    </td>
                    <td style={{width: '40%'}}>
                        <Link to={`/view/${d.ID}`}>
                            <Button style={{marginRight: 3}} variant="info">VIEW</Button>
                        </Link>
                        <Link to={`/edit/${d.ID}`}>
                            <Button style={{marginRight: 3}} variant="warning">EDIT</Button>
                        </Link>
                        <Button variant="danger" onClick={(id)=> this.handleDelete(d.ID)}>DELETE</Button>
                    </td>
                </tr>
            </tbody>
        )
        return (
            <Container>
                <h1>Article Management</h1>
                <Link to='/add'>
                    <Button variant="dark">Add New Article</Button>
                </Link>
                <Table striped bordered hover style={{marginTop: 50}}>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>TITLE</th>
                            <th>DESCRIPTION</th>
                            <th>CREATED_DATE</th>
                            <th>IMAGE</th>
                            <th>ACTION</th>
                        </tr>
                    </thead>
                        {myData}
                </Table>
                <div style={{marginLeft: '35%'}}>
                <Pagination
                    prevPageText='prev'
                    nextPageText='next'
                    firstPageText='first'
                    lastPageText='last'
                    activePage={this.state.activePage}
                    itemsCountPerPage={this.state.perPage}
                    totalItemsCount={this.state.data.DATA.length}
                    itemClass="page-item"
                    linkClass="page-link"
                    onChange={this.handlePageChange.bind(this)}
                 />
                 </div>
            </Container>
        )  
    }
}
