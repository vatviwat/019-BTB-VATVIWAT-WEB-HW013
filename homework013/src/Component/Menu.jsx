import React, { Component } from 'react'
import {Link} from 'react-router-dom'
import {Navbar,Form,Nav,Container,Button,FormControl} from 'react-bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css';

export default class Menu extends Component {
    constructor(){
        super();
        this.state = {
            search: ''
        }
    }
    handleSearch=(e)=>{
        const s = e.target.value
        this.setState({search: s})
    }
    render() {
        return (
            <Navbar bg="dark" variant="dark">
                <Container>
                <Navbar.Brand as={Link} to='/'>AMS</Navbar.Brand>
                <Nav className="mr-auto">
                    <Nav.Link as={Link} to="#">Home</Nav.Link>
                </Nav>
                <Form inline>
                    <FormControl type="text" placeholder="Search" className="mr-sm-2" onChange={this.handleSearch}/>
                    <Link to={`/search/${this.state.search}`}>
                        <Button variant="outline-info">Search</Button>
                    </Link>
                </Form>
                </Container>
            </Navbar>
        )
    }
}
