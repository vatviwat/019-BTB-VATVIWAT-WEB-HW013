import React from 'react'
import { Container, Row, Col, Image, Card,Button} from 'react-bootstrap';
import {Link} from 'react-router-dom'
import axios from 'axios'
export default class View extends React.Component {
    constructor(props){
        super(props);
        this.state={
            data: {
                DATA: []
            },
            TITLE: '',
            DESCRIPTION: '',
            IMAGE: ''
        }
    }
    componentWillMount(){
        const ID = this.props.match.params.id;
        console.log(this.props.match.params.id);
        axios.get(`http://110.74.194.124:15011/v1/api/articles/${ID}`).then(res =>{
            this.setState({
                data: res.data,
                TITLE: res.data.DATA.TITLE,
                DESCRIPTION: res.data.DATA.DESCRIPTION,
                IMAGE: res.data.DATA.IMAGE
            });
        });
    }
    render(){
        return (
            <Container style={{marginTop: '5%',textAlign: 'start'}}>
                <h1>Article Content</h1>
                <Card>
                <Row>
                    <Col lg="4">
                        <div>
                            <Image src={this.state.IMAGE} alt="no image" thumbnail/>
                        </div>
                    </Col>
                    <Col lg="8">
                        <div style={{marginTop: '3%'}}>
                            <h3 style={{marginRight: '2%'}}>{this.state.TITLE}</h3>
                            <p style={{marginTop: '2%',marginRight: '2%'}}>{this.state.DESCRIPTION}</p>
                        </div>
                    </Col>
                </Row>
                </Card>
                <div style={{textAlign: "start", marginTop: '2%'}}>
                    <Link to='/'><Button variant="dark">Back</Button></Link>
                </div>
            </Container>
        )
    }
}

